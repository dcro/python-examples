#!/usr/bin/env python3

"""
A small demonstration of tqdm and multiprocessing.

Try executing the script with a very large --count parameter so you can see the progress bar get incrementally updated
(otherwise you'll mostly be seeing multiprocessing buffer the job requests). eg,
    ./multiprocessing_tqdm.py --count 65536 --duration 0.5

You can always ctrl-c to kill it midway through.

I use imap_unordered and chunksize=1 here purely because it's slightly more likely to yield the results
back to use more frequently, giving a better sense of the tqdm output.
"""

from typing import Tuple, Generator

from tqdm import tqdm

import multiprocessing
import time
import random


def approximate_sleep(idx: int, duration: float) -> int:
    """
    Sleep for approximately the supplied duration.

    :param idx: An ID for this job
    :param duration: The number of seconds to sleep
    :return: The job ID
    """
    lo = duration * 0.9
    hi = duration * 1.1
    duration = random.uniform(lo, hi)
    time.sleep(duration)
    return idx


def _expand_approximate_sleep(args: Tuple[int, float]) -> int:
    """
    A convenience function that receives a tuple of arguments to expand into a call to approximate_sleep
    """
    idx, duration = args
    return approximate_sleep(idx, duration)


def job_list(count: int, duration: float) -> Generator[Tuple[int,float], None, None]:
    """
    Yield a sequence of job definitions that will be executed using multiprocessing.

    :param count: The number of jobs to generated
    :param duration:
    :return:
    """
    for idx in range(count):
        yield idx, duration


def runner(count: int, duration: float):
    # Remember to supply an explicit 'total' here because we're giving it a generator, not a list, and so it can't
    # determine the length of the sequence.
    jobs = tqdm(job_list(count, duration), total=count)

    with multiprocessing.Pool() as p:
        # Use imap_unordered and chunksize=1 because it's likely to yield items more frequently and makes printing to
        # the console more interesting.
        #
        # Note: chunksize=1 is a bad idea for small jobs as the multiprocessing overhead tends to dominate the runtime.
        for idx in p.imap_unordered(_expand_approximate_sleep, jobs, chunksize=1):
            # If we use 'print' then the progressbar display gets disrupted a little, by using tqdm.write the
            # progressbar will get redrawn after the supplied text.
            #
            # It's not a requirement. It just looks tidier.
            tqdm.write(f"Finished {idx}")


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--count', type=int, required=True, help="The number of jobs to queue")
    parser.add_argument('--duration', type=float, required=True, help="The approximate time to sleep, in seconds")
    args = parser.parse_args()

    runner(args.count, args.duration)
